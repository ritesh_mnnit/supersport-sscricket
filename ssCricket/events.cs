﻿using System;
using System.Configuration;

namespace ssCricket
{
    public class EventConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }

    [ConfigurationCollection(typeof(EventConfig))]
    public class EventConfigCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new EventConfig();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((EventConfig)element).Name;
        }
    }

    public class EventConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("events", IsDefaultCollection = true)]
        public EventConfigCollection Events
        {
            get { return (EventConfigCollection)this["events"]; }
            set { this["events"] = value; }
        }
    }
}
