﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using Common.Logging;

namespace ssCricket
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        runInfo curRun = new runInfo();
        DateTime sheduleLastRun = DateTime.Now.AddMinutes(-20);
        EventConfigSection config;

        public string proxyUsername;
        public string proxyPassword;
        public string proxyDomain;
        public string proxyAddress;
        public int proxyPort;

        private ILog _logger = null;

        public Service1()
        {
            _logger = LogManager.GetLogger(typeof(Service1));

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            proxyUsername = Sw.ReadLine();
            proxyPassword = Sw.ReadLine();
            proxyDomain = Sw.ReadLine();
            proxyAddress = Sw.ReadLine();
            proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        protected void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
                dbConnected = true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in run()", ex);

                dbConnected = false;
            }

            if (dbConnected && dbConn != null)
            {
                try
                {
                    dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in run()", ex);

                    curRun.Errors.Add(ex.Message.ToString());
                }

                TimeSpan timeSpan = DateTime.Now.Subtract(sheduleLastRun);
                if (timeSpan.TotalMinutes > 10)
                {
                    Schedule();
                }

                Scorecards();

                try
                {
                    dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in run()", ex);

                    curRun.Errors.Add(ex.Message.ToString());
                }
            }
            else
            {
                curRun.Errors.Add("Failed to connect to the database");
            }

            curRun.End = DateTime.Now;
            endRun();
            writeInfo();

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(60000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into cricket.dbo.cricinfo_runs (startTime, endTime, downloads, downloadErrors, errors, timeStamp) Values (@startTime, @endTime, @downloads, @downloadErrors, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@downloads", SqlDbType.Int).Value = curRun.Downloads;
                SqlQuery.Parameters.Add("@downloadErrors", SqlDbType.Int).Value = curRun.DownloadErrors;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in endRun()", ex);

                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private void writeInfo()
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Downloads: " + curRun.Downloads.ToString());
                Sw.WriteLine("Download Errors: " + curRun.DownloadErrors.ToString());
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();

                if (curRun.Errors.Count > 0)
                {
                    System.Net.WebProxy tmpProxy;
                    System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
                    tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
                    tmpProxy.Credentials = ProxyCredentials;
                    errors.Service tmpErrors = new errors.Service();
                    tmpErrors.Proxy = tmpProxy;
                    tmpErrors.Errors("Cricket", "Cricinfo", "Error while running cricinfo feeds", 3, "Error while running cricinfo feeds", false, "");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in writeInfo()", ex);
            }
        }

        private void Schedule()
        {
            string scheduleText = getFile("schedule.xml");
            if (!(string.IsNullOrEmpty(scheduleText)))
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(scheduleText);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in Schedule()", ex);

                    xmlDoc = null;
                    curRun.Errors.Add("Xml is invalid - Schedule.xml");
                }

                if (xmlDoc != null)
                {
                    foreach (XmlNode matchNode in xmlDoc.SelectNodes("matches/match"))
                    {
                        int id = -1;
                        string title = string.Empty;
                        int seriesId = -1;
                        string series = string.Empty;
                        int typeId = -1;
                        string type = string.Empty;
                        DateTime startDate = new DateTime(1900, 1, 1);
                        DateTime endDate = new DateTime(1900, 1, 1);
                        DateTime startTime = new DateTime(1900, 1, 1);
                        int homeTeamId = -1;
                        string homeTeamName = string.Empty;
                        string homeTeamAbbr = string.Empty;
                        int awayTeamId = -1;
                        string awayTeamName = string.Empty;
                        string awayTeamAbbr = string.Empty;
                        int venueId = -1;
                        string venueName = string.Empty;
                        string venueSmallName = string.Empty;

                        try
                        {
                            foreach (XmlAttribute xmlAttribute in matchNode.Attributes)
                            {
                                switch (xmlAttribute.Name)
                                {
                                    case "id":
                                        id = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "title":
                                        title = xmlAttribute.Value.ToString();
                                        break;
                                    case "series_id":
                                        seriesId = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "series":
                                        series = xmlAttribute.Value.ToString();
                                        break;
                                    case "type_id":
                                        typeId = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "type":
                                        type = xmlAttribute.Value.ToString();
                                        break;
                                    case "startdate":
                                        startDate = Convert.ToDateTime(xmlAttribute.Value);
                                        break;
                                    case "enddate":
                                        endDate = Convert.ToDateTime(xmlAttribute.Value);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            series = series.Replace("'", "''");

                            foreach (XmlNode homeTeamNode in matchNode.SelectNodes("hometeam"))
                            {
                                foreach (XmlAttribute xmlAttribute in homeTeamNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            bool result = Int32.TryParse(xmlAttribute.Value, out homeTeamId);
                                            break;
                                        case "name":
                                            homeTeamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        case "abbr":
                                            homeTeamAbbr = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode awayTeamNode in matchNode.SelectNodes("awayteam"))
                            {
                                foreach (XmlAttribute xmlAttribute in awayTeamNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            bool result = Int32.TryParse(xmlAttribute.Value, out awayTeamId);
                                            break;
                                        case "name":
                                            awayTeamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        case "abbr":
                                            awayTeamAbbr = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode venueNode in matchNode.SelectNodes("venue"))
                            {
                                foreach (XmlAttribute xmlAttribute in venueNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            venueId = Convert.ToInt32(xmlAttribute.Value);
                                            break;
                                        case "name":
                                            venueName = xmlAttribute.Value.ToString();
                                            break;
                                        case "small_name":
                                            venueSmallName = xmlAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode starTimeNode in matchNode.SelectNodes("starttime"))
                            {
                                try
                                {
                                    string startTimeText = starTimeNode.InnerText.Replace("GMT", "").Trim(); ;
                                    string[] startTimes = startTimeText.Split(' ')[0].Split(':');
                                    startTime = new DateTime(1900, 1, 1, Convert.ToInt32(startTimes[0]), Convert.ToInt32(startTimes[1]), 0);
                                    if (startTime.Hour >= 22)
                                    {
                                        startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTime.Hour, startTime.Minute, 0);
                                        startDate = startDate.AddHours(2);
                                        startDate = startDate.AddDays(-1);
                                    }
                                    else
                                    {
                                        startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTime.Hour, startTime.Minute, 0);
                                        startDate = startDate.AddHours(2);
                                    }
                                }
                                catch(Exception ex)
                                {
                                    _logger.Error("Error occurred in Schedule() -> foreach starttime select nodes", ex);
                                }
                            }

                            bool insert = true;

                            SqlCommand ignoreList = new SqlCommand("SELECT COUNT(Name) FROM cricket.dbo.ignore_tournaments where Name = @Name", dbConn);
                            ignoreList.Parameters.Add("@Name", SqlDbType.VarChar).Value = series;
                            int count = Convert.ToInt32(ignoreList.ExecuteScalar().ToString());

                            if (count > 0)
                            {
                                insert = false;

                                SqlCommand exists = new SqlCommand("SELECT COUNT(Series) FROM cricket.dbo.scorecard_matches where Series = @Series", dbConn);
                                exists.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                int nrMatches = Convert.ToInt32(exists.ExecuteScalar().ToString());

                                if (nrMatches > 0)
                                {
                                    SqlCommand deleteIgnoredTournaments = new SqlCommand("DELETE FROM cricket.dbo.scorecard_matches where Series = @Series", dbConn);
                                    deleteIgnoredTournaments.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                    deleteIgnoredTournaments.ExecuteNonQuery();
                                }
                            }

                            if (insert)
                            {
                                SqlCommand SqlQuery = new SqlCommand("SELECT Count(Id) FROM cricket.dbo.scorecard_matches WHERE (Id = @id)", dbConn);
                                SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id;
                                int present = Convert.ToInt32(SqlQuery.ExecuteScalar());
                                if (present <= 0)
                                {
                                    SqlQuery = new SqlCommand("INSERT INTO cricket.dbo.scorecard_matches (Id ,Type ,Series ,HomeTeamId ,AwayTeamId ,HomeTeamName ,AwayTeamName ,HomeTeamAbbr ,AwayTeamAbbr ,VenueId ,VenueName ,StartDate ,EndDate ,Updated ,Created ,title ,seriesId ,typeId) VALUES (@Id ,@Type, @Series ,@HomeTeamId ,@AwayTeamId ,@HomeTeamName ,@AwayTeamName ,@HomeTeamAbbr ,@AwayTeamAbbr ,@VenueId ,@VenueName ,@StartDate ,@EndDate ,GetDate() ,GetDate() ,@title ,@seriesId ,@typeId)", dbConn);
                                    SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                                    SqlQuery.Parameters.Add("@Type", SqlDbType.VarChar).Value = type;
                                    SqlQuery.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                    SqlQuery.Parameters.Add("@HomeTeamId", SqlDbType.Int).Value = homeTeamId;
                                    SqlQuery.Parameters.Add("@AwayTeamId", SqlDbType.Int).Value = awayTeamId;
                                    SqlQuery.Parameters.Add("@HomeTeamName", SqlDbType.VarChar).Value = homeTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamName", SqlDbType.VarChar).Value = awayTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@HomeTeamAbbr", SqlDbType.VarChar).Value = homeTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamAbbr", SqlDbType.VarChar).Value = awayTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@VenueId", SqlDbType.Int).Value = venueId;
                                    SqlQuery.Parameters.Add("@VenueName", SqlDbType.VarChar).Value = venueName;
                                    SqlQuery.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                                    SqlQuery.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = GetEndDate(startDate, endDate, type);
                                    SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
                                    SqlQuery.Parameters.Add("@seriesId", SqlDbType.Int).Value = seriesId;
                                    SqlQuery.Parameters.Add("@typeId", SqlDbType.Int).Value = typeId;
                                    SqlQuery.ExecuteNonQuery();
                                }
                                else
                                {
                                    SqlQuery = new SqlCommand("UPDATE cricket.dbo.scorecard_matches Set Type = @Type ,Series = @Series ,HomeTeamId = @HomeTeamId,AwayTeamId = @AwayTeamId,HomeTeamName = @HomeTeamName,AwayTeamName = @AwayTeamName,HomeTeamAbbr = @HomeTeamAbbr,AwayTeamAbbr = @AwayTeamAbbr,VenueId = @VenueId,VenueName = @VenueName,StartDate = @StartDate,EndDate = @EndDate,Updated = GetDate(),title = @title,seriesId = @seriesId,typeId = @typeId WHERE Id = @Id", dbConn);
                                    SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                                    SqlQuery.Parameters.Add("@Type", SqlDbType.VarChar).Value = type;
                                    SqlQuery.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                    SqlQuery.Parameters.Add("@HomeTeamId", SqlDbType.Int).Value = homeTeamId;
                                    SqlQuery.Parameters.Add("@AwayTeamId", SqlDbType.Int).Value = awayTeamId;
                                    SqlQuery.Parameters.Add("@HomeTeamName", SqlDbType.VarChar).Value = homeTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamName", SqlDbType.VarChar).Value = awayTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@HomeTeamAbbr", SqlDbType.VarChar).Value = homeTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamAbbr", SqlDbType.VarChar).Value = awayTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@VenueId", SqlDbType.Int).Value = venueId;
                                    SqlQuery.Parameters.Add("@VenueName", SqlDbType.VarChar).Value = venueName;
                                    SqlQuery.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                                    SqlQuery.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = GetEndDate(startDate, endDate, type);
                                    SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
                                    SqlQuery.Parameters.Add("@seriesId", SqlDbType.Int).Value = seriesId;
                                    SqlQuery.Parameters.Add("@typeId", SqlDbType.Int).Value = typeId;
                                    SqlQuery.ExecuteNonQuery();
                                }

                                updateFixtures(startDate, homeTeamName.Replace("T20", "").Trim(), id);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Error occurred in Schedule()", ex);

                            curRun.Errors.Add("Error running schedule id:" + id.ToString());
                        }

                    }
                }
            }

            sheduleLastRun = DateTime.Now;
        }

        private DateTime GetEndDate(DateTime startDate, DateTime endDate, string type)
        {
            //return (type.ToLower()).StartsWith("odi") ? startDate.Add(new TimeSpan(8, 0, 0)) : endDate;
            if ((type.ToLower()).StartsWith("odi") || (type.ToLower()).StartsWith("t20"))
                return startDate.Add(new TimeSpan(8, 0, 0));
            if ((type.ToLower()).StartsWith("test"))
                return endDate.Add(new TimeSpan(23, 59, 0));
            return endDate;

        }

        private void Scorecards()
        {
            string scheduleText = getFile("scorecard.xml");
            ArrayList scoreCards = new ArrayList();
            if (!(string.IsNullOrEmpty(scheduleText)))
            {
                XmlDocument xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(scheduleText);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in Scorecards()", ex);

                    xmlDoc = null;
                    curRun.Errors.Add("Xml is invalid - Schedule.xml");
                }

                if (xmlDoc != null)
                {
                    foreach (XmlNode matchNode in xmlDoc.SelectNodes("matches/match"))
                    {
                        int id = -1;
                        string title = string.Empty;
                        int seriesId = -1;
                        string series = string.Empty;
                        int typeId = -1;
                        string type = string.Empty;
                        DateTime startDate = new DateTime(1900, 1, 1);
                        DateTime endDate = new DateTime(1900, 1, 1);
                        DateTime startTime = new DateTime(1900, 1, 1);
                        int homeTeamId = -1;
                        string homeTeamName = string.Empty;
                        string homeTeamAbbr = string.Empty;
                        int awayTeamId = -1;
                        string awayTeamName = string.Empty;
                        string awayTeamAbbr = string.Empty;
                        int venueId = -1;
                        string venueName = string.Empty;
                        string venueSmallName = string.Empty;

                        try
                        {
                            foreach (XmlAttribute xmlAttribute in matchNode.Attributes)
                            {
                                switch (xmlAttribute.Name)
                                {
                                    case "id":
                                        id = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "title":
                                        title = xmlAttribute.Value.ToString();
                                        break;
                                    case "series_id":
                                        seriesId = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "series":
                                        series = xmlAttribute.Value.ToString();
                                        if (series.ToLower() == "chappell-hadlee trophy"
                                            && DateTime.Now > new DateTime(2015, 2, 14)
                                            && DateTime.Now < new DateTime(2015, 3, 30))
                                        {
                                            series = "ICC Cricket World Cup";
                                        }
                                        break;
                                    case "type_id":
                                        typeId = Convert.ToInt32(xmlAttribute.Value);
                                        break;
                                    case "type":
                                        type = xmlAttribute.Value.ToString();
                                        break;
                                    case "startdate":
                                        startDate = Convert.ToDateTime(xmlAttribute.Value);
                                        break;
                                    case "enddate":
                                        endDate = Convert.ToDateTime(xmlAttribute.Value);
                                        break;
                                    case "startTime":
                                        try
                                        {
                                            string startTimeText = xmlAttribute.Value.Replace("GMT", "").Trim();
                                            string[] startTimes = startTimeText.Split(' ')[0].Split(':');
                                            startTime = new DateTime(1900, 1, 1, Convert.ToInt32(startTimes[0]), Convert.ToInt32(startTimes[1]), 0);
                                            if (startTime.Hour >= 22)
                                            {
                                                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTime.Hour, startTime.Minute, 0);
                                                startDate = startDate.AddHours(2);
                                                startDate = startDate.AddDays(-1);
                                            }
                                            else
                                            {
                                                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTime.Hour, startTime.Minute, 0);
                                                startDate = startDate.AddHours(2);
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            _logger.Error("Error occurred in Scorecards()", ex);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            series = series.Replace("'", "''");

                            foreach (XmlNode homeTeamNode in matchNode.SelectNodes("hometeam"))
                            {
                                foreach (XmlAttribute xmlAttribute in homeTeamNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            bool result = Int32.TryParse(xmlAttribute.Value, out homeTeamId);
                                            break;
                                        case "name":
                                            homeTeamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        case "abbr":
                                            homeTeamAbbr = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode awayTeamNode in matchNode.SelectNodes("awayteam"))
                            {
                                foreach (XmlAttribute xmlAttribute in awayTeamNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            bool result = Int32.TryParse(xmlAttribute.Value, out awayTeamId);
                                            break;
                                        case "name":
                                            awayTeamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        case "abbr":
                                            awayTeamAbbr = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode venueNode in matchNode.SelectNodes("venue"))
                            {
                                foreach (XmlAttribute xmlAttribute in venueNode.Attributes)
                                {
                                    switch (xmlAttribute.Name)
                                    {
                                        case "id":
                                            venueId = Convert.ToInt32(xmlAttribute.Value);
                                            break;
                                        case "name":
                                            venueName = xmlAttribute.Value.ToString();
                                            break;
                                        case "small_name":
                                            venueSmallName = xmlAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            foreach (XmlNode starTimeNode in matchNode.SelectNodes("starttime"))
                            {
                                try
                                {
                                    string startTimeText = starTimeNode.InnerText.Replace("GMT", "").Trim();
                                    string[] startTimes = startTimeText.Split(' ')[0].Split(':');
                                    startTime = new DateTime(1900, 1, 1, Convert.ToInt32(startTimes[0]), Convert.ToInt32(startTimes[1]), 0);
                                    startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, startTime.Hour, startTime.Minute, 0);
                                    startDate = startDate.AddHours(2);
                                }
                                catch(Exception ex)
                                {
                                    _logger.Error("Error occurred in Scorecards() -> foreach starttime select nodes", ex);
                                }
                            }

                            bool insert = true;

                            SqlCommand ignoreList = new SqlCommand("SELECT COUNT(Name) FROM cricket.dbo.ignore_tournaments where Name = @Name", dbConn);
                            ignoreList.Parameters.Add("@Name", SqlDbType.VarChar).Value = series;
                            int count = Convert.ToInt32(ignoreList.ExecuteScalar().ToString());

                            if (count > 0)
                            {
                                insert = false;
                            }

                            if (insert)
                            {
                                SqlCommand SqlQuery = new SqlCommand("SELECT Count(Id) FROM cricket.dbo.scorecard_matches WHERE (Id = @id)", dbConn);
                                SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id;
                                int present = Convert.ToInt32(SqlQuery.ExecuteScalar());
                                if (present <= 0)
                                {
                                    SqlQuery = new SqlCommand("INSERT INTO cricket.dbo.scorecard_matches (Id ,Type ,Series ,HomeTeamId ,AwayTeamId ,HomeTeamName ,AwayTeamName ,HomeTeamAbbr ,AwayTeamAbbr ,VenueId ,VenueName ,StartDate ,EndDate ,Updated ,Created ,title ,seriesId ,typeId) VALUES (@Id ,@Type, @Series ,@HomeTeamId ,@AwayTeamId ,@HomeTeamName ,@AwayTeamName ,@HomeTeamAbbr ,@AwayTeamAbbr ,@VenueId ,@VenueName ,@StartDate ,@EndDate ,GetDate() ,GetDate() ,@title ,@seriesId ,@typeId)", dbConn);
                                    SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                                    SqlQuery.Parameters.Add("@Type", SqlDbType.VarChar).Value = type;
                                    SqlQuery.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                    SqlQuery.Parameters.Add("@HomeTeamId", SqlDbType.Int).Value = homeTeamId;
                                    SqlQuery.Parameters.Add("@AwayTeamId", SqlDbType.Int).Value = awayTeamId;
                                    SqlQuery.Parameters.Add("@HomeTeamName", SqlDbType.VarChar).Value = homeTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamName", SqlDbType.VarChar).Value = awayTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@HomeTeamAbbr", SqlDbType.VarChar).Value = homeTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamAbbr", SqlDbType.VarChar).Value = awayTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@VenueId", SqlDbType.Int).Value = venueId;
                                    SqlQuery.Parameters.Add("@VenueName", SqlDbType.VarChar).Value = venueName;
                                    //SqlQuery.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                                    //SqlQuery.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = GetEndDate(startDate,endDate,type);
                                    SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
                                    SqlQuery.Parameters.Add("@seriesId", SqlDbType.Int).Value = seriesId;
                                    SqlQuery.Parameters.Add("@typeId", SqlDbType.Int).Value = typeId;
                                    SqlQuery.ExecuteNonQuery();
                                }
                                else
                                {
                                    SqlQuery = new SqlCommand("UPDATE cricket.dbo.scorecard_matches Set Type = @Type ,Series = @Series ,HomeTeamId = @HomeTeamId,AwayTeamId = @AwayTeamId,HomeTeamName = @HomeTeamName,AwayTeamName = @AwayTeamName,HomeTeamAbbr = @HomeTeamAbbr,AwayTeamAbbr = @AwayTeamAbbr,VenueId = @VenueId,VenueName = @VenueName,Updated = GetDate(),title = @title,seriesId = @seriesId,typeId = @typeId WHERE Id = @Id", dbConn);
                                    SqlQuery.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                                    SqlQuery.Parameters.Add("@Type", SqlDbType.VarChar).Value = type;
                                    SqlQuery.Parameters.Add("@Series", SqlDbType.VarChar).Value = series;
                                    SqlQuery.Parameters.Add("@HomeTeamId", SqlDbType.Int).Value = homeTeamId;
                                    SqlQuery.Parameters.Add("@AwayTeamId", SqlDbType.Int).Value = awayTeamId;
                                    SqlQuery.Parameters.Add("@HomeTeamName", SqlDbType.VarChar).Value = homeTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamName", SqlDbType.VarChar).Value = awayTeamName.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@HomeTeamAbbr", SqlDbType.VarChar).Value = homeTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@AwayTeamAbbr", SqlDbType.VarChar).Value = awayTeamAbbr.Replace("T20", "").Trim();
                                    SqlQuery.Parameters.Add("@VenueId", SqlDbType.Int).Value = venueId;
                                    SqlQuery.Parameters.Add("@VenueName", SqlDbType.VarChar).Value = venueName;
                                    //SqlQuery.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = startDate;
                                    //SqlQuery.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = GetEndDate(startDate, endDate, type);
                                    SqlQuery.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
                                    SqlQuery.Parameters.Add("@seriesId", SqlDbType.Int).Value = seriesId;
                                    SqlQuery.Parameters.Add("@typeId", SqlDbType.Int).Value = typeId;
                                    SqlQuery.ExecuteNonQuery();
                                }

                                scoreCards.Add(id);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error("Error occurred in Scorecards()", ex);

                            curRun.Errors.Add("Error with match detail for id:" + id.ToString() + " : " + ex.Message );
                        }
                    }
                }
            }

            foreach (int curId in scoreCards)
            {
                string scorecardText = getFile("scorecard/" + curId.ToString() + ".xml");
                if (!(string.IsNullOrEmpty(scorecardText)))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(scorecardText);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in Scorecards()", ex);

                        xmlDoc = null;
                        curRun.Errors.Add("Xml is invalid - " + curId.ToString() + ".xml");
                    }

                    if (xmlDoc != null)
                    {
                        Scorecard(xmlDoc);
                    }
                }
            }
        }

        private void Scorecard(XmlDocument xmlDoc)
        {
            SqlCommand SqlQuery;
            Hashtable teams = new Hashtable();
            Hashtable players = new Hashtable();
            Hashtable innings = new Hashtable();
            int matchId = -1;
            string homeTeamName = string.Empty;
            string awayTeamName = string.Empty;
            string curTarget = string.Empty;
            string curOvers = string.Empty;
            StringBuilder sb = new StringBuilder();
            bool result = false;
            DateTime startDate = DateTime.Now;
            SqlDataReader RsRec;
            int Overwrite = 0;

            try
            {
                foreach (XmlNode matchNode in xmlDoc.SelectNodes("scorecard/match"))
                {
                    matchId = Convert.ToInt32(matchNode.Attributes["id"].Value);

                    SqlQuery = new SqlCommand("SELECT HomeTeamId, HomeTeamName, AwayTeamId, AwayTeamName, StartDate FROM cricket.dbo.Scorecard_Matches WHERE (Id = @id)", dbConn);
                    SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = matchId;
                    RsRec = SqlQuery.ExecuteReader();
                    while (RsRec.Read())
                    {
                        int homeTeamId = Convert.ToInt32(RsRec["HomeTeamId"]);
                        int awayTeamId = Convert.ToInt32(RsRec["AwayTeamId"]);
                        homeTeamName = RsRec["HomeTeamName"].ToString();
                        awayTeamName = RsRec["AwayTeamName"].ToString();
                        teams.Add(homeTeamId, homeTeamName);
                        teams.Add(awayTeamId, awayTeamName);
                        startDate = Convert.ToDateTime(RsRec["StartDate"]);
                    }
                    RsRec.Close();
                }

                updateFixtures(startDate, homeTeamName, matchId);

                sb.Append("DELETE FROM Cricket.dbo.Scorecard_lineups WHERE (matchId = " + matchId + ");");

                foreach (XmlNode teamNode in xmlDoc.SelectNodes("scorecard/umpire"))
                {
                    string ump1 = string.Empty;
                    string ump2 = string.Empty;
                    string ump3 = string.Empty;
                    string ump4 = string.Empty;

                    foreach (XmlAttribute xmlAttribute in teamNode.Attributes)
                    {
                        switch (xmlAttribute.Name)
                        {
                            case "ground_umpire1":
                                ump1 = xmlAttribute.Value.ToString();
                                break;
                            case "ground_umpire2":
                                ump2 = xmlAttribute.Value.ToString();
                                break;
                            case "third_umpire":
                                ump3 = xmlAttribute.Value.ToString();
                                break;
                            case "referee":
                                ump4 = xmlAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }

                        sb.Append("UPDATE cricket.dbo.scorecard_Matches Set UmpireA = '" + ump1.Replace("'", "''") + "', UmpireB = '" + ump2.Replace("'", "''") + "', UmpireC = '" + ump3.Replace("'", "''") + "', Referee = '" + ump4.Replace("'", "''") + "' WHERE (Id = " + matchId + ")");
                    }
                }

                foreach (XmlNode teamNode in xmlDoc.SelectNodes("scorecard/team"))
                {
                    int teamId = -1;
                    string teamName = string.Empty;
                    int captain = -1;
                    int keeper = -1;

                    foreach (XmlAttribute xmlAttribute in teamNode.Attributes)
                    {
                        switch (xmlAttribute.Name)
                        {
                            case "id":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                {
                                    teamId = Convert.ToInt32(xmlAttribute.Value);
                                }
                                break;
                            case "keeper":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                {
                                    keeper = Convert.ToInt32(xmlAttribute.Value);
                                }
                                break;
                            case "captain":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                {
                                    captain = Convert.ToInt32(xmlAttribute.Value);
                                }
                                break;
                            case "name":
                                teamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode playerNode in teamNode.SelectNodes("player"))
                    {
                        Player curPlayer = new Player();
                        int playerId = -1;
                        string playerName = string.Empty;
                        string playerSurname = string.Empty;
                        string playerInitials = string.Empty;
                        int isCaptain = 0;
                        int isKeeper = 0;

                        foreach (XmlAttribute xmlAttribute in playerNode.Attributes)
                        {
                            switch (xmlAttribute.Name)
                            {
                                case "id":
                                    if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                    {
                                        playerId = Convert.ToInt32(xmlAttribute.Value);
                                    }
                                    break;
                                case "name":
                                    playerName = xmlAttribute.Value.ToString();
                                    break;
                                case "surname":
                                    playerSurname = xmlAttribute.Value.ToString();
                                    break;
                                case "initials":
                                    playerInitials = xmlAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (playerId > -1)
                        {
                            curPlayer.Id = playerId;
                            curPlayer.Initials = playerInitials;
                            curPlayer.Name = playerName;
                            curPlayer.Surname = playerSurname;
                            curPlayer.TeamId = teamId;
                            curPlayer.TeamName = teamName.Replace("T20", "").Trim();

                            if (captain == playerId)
                            {
                                curPlayer.Captain = true;
                            }
                            if (keeper == playerId)
                            {
                                curPlayer.Keeper = true;
                            }

                            SqlQuery = new SqlCommand("SELECT * FROM cricket.dbo.Scorecard_Players WHERE (Id = @id)", dbConn);
                            SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = playerId;
                            RsRec = SqlQuery.ExecuteReader();
                            while (RsRec.Read())
                            {
                                curPlayer.AltName = RsRec["Name"].ToString();
                                curPlayer.AltSurname = RsRec["Surname"].ToString();
                                curPlayer.AltInitials = RsRec["Initials"].ToString();
                            }
                            RsRec.Close();

                            if (!players.ContainsKey(playerId))
                            {
                                players.Add(playerId, curPlayer);
                                sb.Append("INSERT INTO Cricket.dbo.Scorecard_lineups (MatchId, TeamId, TeamName, Id, Name, AltName, Surname, AltSurname, Initials, AltInitials, Captain, Keeper) VALUES (" + matchId + ", " + curPlayer.TeamId + ", '" + curPlayer.TeamName.Replace("'", "''") + "', " + curPlayer.Id + ", '" + curPlayer.Name.Replace("'", "''") + "', '" + curPlayer.AltName.Replace("'", "''") + "', '" + curPlayer.Surname.Replace("'", "''") + "', '" + curPlayer.AltSurname.Replace("'", "''") + "', '" + curPlayer.Initials.Replace("'", "''") + "', '" + curPlayer.AltInitials.Replace("'", "''") + "', " + Convert.ToInt32(curPlayer.Captain) + ", " + Convert.ToInt32(curPlayer.Keeper) + ");");
                            }
                        }
                    }
                }

                foreach (XmlNode matchNode in xmlDoc.SelectNodes("scorecard/match"))
                {
                    string matchStatus = string.Empty;
                    string matchBreak = string.Empty;
                    int tossWinnerId = -1;
                    string tossWinnerName = string.Empty;
                    int winningTeamId = -1;
                    string winningTeamName = string.Empty;
                    string tossDecision = string.Empty;
                    string resultType = string.Empty;
                    string momIds = string.Empty;
                    string momName = string.Empty;
                    string mosIds = string.Empty;
                    string mosName = string.Empty;

                    foreach (XmlAttribute xmlAttribute in matchNode.Attributes)
                    {
                        switch (xmlAttribute.Name)
                        {
                            case "tosswinner":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                {
                                    tossWinnerId = Convert.ToInt32(xmlAttribute.Value);
                                }
                                break;
                            case "tosswinner_name":
                                tossWinnerName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                break;
                            case "winningteam":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value))
                                {
                                    Int32.TryParse(xmlAttribute.Value.ToString(), out winningTeamId);
                                    if (winningTeamId > 0)
                                    {
                                        winningTeamName = teams[winningTeamId].ToString();
                                    }
                                    else
                                    {
                                        if (teams.ContainsValue(xmlAttribute.Value.ToString().Replace("T20", "").Trim()))
                                        {
                                            winningTeamName = xmlAttribute.Value.ToString().Replace("T20", "").Trim();
                                            foreach (DictionaryEntry Item in teams)
                                            {
                                                if (Item.Value.ToString() == winningTeamName)
                                                {
                                                    winningTeamId = Convert.ToInt32(Item.Key);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case "status":
                                matchStatus = xmlAttribute.Value.ToString();
                                break;
                            case "break":
                                matchBreak = xmlAttribute.Value.ToString();
                                break;
                            case "tossdecision":
                                tossDecision = xmlAttribute.Value.ToString();
                                break;
                            case "mom":
                                momIds = xmlAttribute.Value.ToString();
                                if (!string.IsNullOrEmpty(momIds))
                                {
                                    string[] tmpArray = momIds.Split(',');
                                    for (int i = 0; i <= tmpArray.GetUpperBound(0); i++)
                                    {
                                        if (i == 0)
                                        {
                                            momIds = tmpArray[i];
                                        }
                                        if (!String.IsNullOrEmpty(tmpArray[i].Trim()) && players.ContainsKey(Convert.ToInt32(tmpArray[i].Trim())))
                                        {
                                            if (momName != "")
                                            {
                                                momName += ", ";
                                            }
                                            momName += ((Player)players[Convert.ToInt32(tmpArray[i])]).Name;
                                        }
                                    }
                                }
                                break;
                            case "mos":
                                mosIds = xmlAttribute.Value.ToString();
                                if (!string.IsNullOrEmpty(mosIds))
                                {
                                    string[] tmpArray = mosIds.Split(',');
                                    for (int i = 0; i <= tmpArray.GetUpperBound(0); i++)
                                    {
                                        if (i == 0)
                                        {
                                            mosIds = tmpArray[i];
                                        }
                                        if (!String.IsNullOrEmpty(tmpArray[i].Trim()) && players.ContainsKey(Convert.ToInt32(tmpArray[i].Trim())))
                                        {
                                            if (mosName != "")
                                            {
                                                mosName += ", ";
                                            }
                                            mosName += ((Player)players[Convert.ToInt32(tmpArray[i])]).Name;
                                        }
                                    }
                                }
                                break;
                            case "resulttype":
                                resultType = xmlAttribute.Value.ToString();
                                if (!string.IsNullOrEmpty(resultType))
                                {
                                    result = true;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    //Check if Score is Override by CMS
                    SqlQuery = new SqlCommand("SELECT Overwrite FROM cricket.dbo.Scorecard_Matches WHERE (Id = @id)", dbConn);
                    SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = matchId;
                    RsRec = SqlQuery.ExecuteReader();
                    while (RsRec.Read())
                    {
                        Overwrite = Convert.ToInt32(RsRec["Overwrite"]);
                    }
                    RsRec.Close();
                    
                    if (result)
                    {
                        matchBreak = "";
                        if (resultType.ToLower() == "match tied")
                        {
                            matchStatus = "Match tied";
                        }
                        if(Overwrite==0)
                            sb.Append("UPDATE cricket.dbo.Scorecard_Matches Set TossWinnerId = " + tossWinnerId + ", TossWinnerName = '" + tossWinnerName.Replace("'", "''") + "', TossDecision = '" + tossDecision.Replace("'", "''") + "', MatchStatus = '" + matchStatus.Replace("'", "''") + "', CricStatus = '" + matchStatus.Replace("'", "''") + "', MatchBreak = '" + matchBreak.Replace("'", "''") + "', Result = '" + result + "', ResultType = '" + resultType.Replace("'", "''") + "', WinningTeamId = " + winningTeamId + ", winningTeamName = '" + winningTeamName.Replace("'", "''") + "', MOMId = '" + momIds.Replace("'", "''") + "', MOSId = '" + mosIds.Replace("'", "''") + "', MOMName = '" + momName.Replace("'", "''") + "', MOSName = '" + mosName.Replace("'", "''") + "', Updated = GetDate() WHERE (Id = " + matchId + ") ;");
                        else
                            sb.Append("UPDATE cricket.dbo.Scorecard_Matches Set TossWinnerId = " + tossWinnerId + ", TossWinnerName = '" + tossWinnerName.Replace("'", "''") + "', TossDecision = '" + tossDecision.Replace("'", "''") + "', CricStatus = '" + matchStatus.Replace("'", "''") + "', MatchBreak = '" + matchBreak.Replace("'", "''") + "',  ResultType = '" + resultType.Replace("'", "''") + "', WinningTeamId = " + winningTeamId + ", winningTeamName = '" + winningTeamName.Replace("'", "''") + "', MOMId = '" + momIds.Replace("'", "''") + "', MOSId = '" + mosIds.Replace("'", "''") + "', MOMName = '" + momName.Replace("'", "''") + "', MOSName = '" + mosName.Replace("'", "''") + "', Updated = GetDate() WHERE (Id = " + matchId + ") ;");

                    }
                    else
                    {
                        if (Overwrite == 0)
                            sb.Append("UPDATE cricket.dbo.Scorecard_Matches Set TossWinnerId = " + tossWinnerId + ", TossWinnerName = '" + tossWinnerName.Replace("'", "''") + "', TossDecision = '" + tossDecision.Replace("'", "''") + "', CricStatus = '" + matchStatus.Replace("'", "''") + "', MatchBreak = '" + matchBreak.Replace("'", "''") + "', Result = '" + result + "', ResultType = '" + resultType.Replace("'", "''") + "', WinningTeamId = " + winningTeamId + ", winningTeamName = '" + winningTeamName.Replace("'", "''") + "', MOMId = '" + momIds.Replace("'", "''") + "', MOSId = '" + mosIds.Replace("'", "''") + "', MOMName = '" + momName.Replace("'", "''") + "', MOSName = '" + mosName.Replace("'", "''") + "', Updated = GetDate() WHERE (Id = " + matchId + ");");
                        else
                            sb.Append("UPDATE cricket.dbo.Scorecard_Matches Set TossWinnerId = " + tossWinnerId + ", TossWinnerName = '" + tossWinnerName.Replace("'", "''") + "', TossDecision = '" + tossDecision.Replace("'", "''") + "', CricStatus = '" + matchStatus.Replace("'", "''") + "', MatchBreak = '" + matchBreak.Replace("'", "''") + "',  ResultType = '" + resultType.Replace("'", "''") + "', WinningTeamId = " + winningTeamId + ", winningTeamName = '" + winningTeamName.Replace("'", "''") + "', MOMId = '" + momIds.Replace("'", "''") + "', MOSId = '" + mosIds.Replace("'", "''") + "', MOMName = '" + momName.Replace("'", "''") + "', MOSName = '" + mosName.Replace("'", "''") + "', Updated = GetDate() WHERE (Id = " + matchId + ");");

                    }
                }

                foreach (XmlNode currentNode in xmlDoc.SelectNodes("scorecard/currentscores"))
                {
                    string inning = string.Empty;
                    string battingId = string.Empty;
                    string battingName = string.Empty;
                    string bowlingId = string.Empty;
                    string bowlingName = string.Empty;
                    string runs = string.Empty;
                    string wickets = string.Empty;
                    string overs = string.Empty;
                    string batting1Id = string.Empty;
                    string batting1Name = "unknown";
                    string batting1Runs = string.Empty;
                    string batting2Id = string.Empty;
                    string batting2Name = "unknown";
                    string batting2Runs = string.Empty;
                    string bowling1Id = string.Empty;
                    string bowling1Name = "unknown";
                    string bowling1Wkts = string.Empty;
                    string bowling2Id = string.Empty;
                    string bowling2Name = "unknown";
                    string bowling2Wkts = string.Empty;
                    string maxOvers = string.Empty;
                    string target = string.Empty;

                    foreach (XmlNode attributeNode in currentNode.ChildNodes)
                    {
                        switch (attributeNode.Name)
                        {
                            case "innings":
                                inning = (Convert.ToInt32(attributeNode.InnerText) + 1).ToString();
                                break;
                            case "batteamname":
                                battingId = attributeNode.InnerText.ToString();
                                break;
                            case "batteam_name":
                                battingName = attributeNode.InnerText.ToString().Replace("T20", "").Trim();
                                break;
                            case "bwlteamname":
                                bowlingId = attributeNode.InnerText.ToString();
                                break;
                            case "bwlteam_name":
                                bowlingName = attributeNode.InnerText.ToString().Replace("T20", "").Trim();
                                break;
                            case "batteamruns":
                                runs = attributeNode.InnerText.ToString();
                                break;
                            case "batteamwkts":
                                wickets = attributeNode.InnerText.ToString();
                                break;
                            case "batteamovers":
                                overs = attributeNode.InnerText.ToString();
                                break;
                            case "bat1":
                                if (!string.IsNullOrEmpty(attributeNode.InnerText.ToString()))
                                {
                                    batting1Id = attributeNode.InnerText.ToString();
                                    if (players.ContainsKey(Convert.ToInt32(batting1Id)))
                                    {
                                        batting1Name = ((Player)players[Convert.ToInt32(batting1Id)]).Name;
                                    }
                                }
                                break;
                            case "bat2":
                                if (!string.IsNullOrEmpty(attributeNode.InnerText.ToString()))
                                {
                                    batting2Id = attributeNode.InnerText.ToString();
                                    if (players.ContainsKey(Convert.ToInt32(batting2Id)))
                                    {
                                        batting2Name = ((Player)players[Convert.ToInt32(batting2Id)]).Name;
                                    }
                                }
                                break;
                            case "bat1runs":
                                batting1Runs = attributeNode.InnerText.ToString();
                                break;
                            case "bat2runs":
                                batting2Runs = attributeNode.InnerText.ToString();
                                break;
                            case "bwl1":
                                if (!string.IsNullOrEmpty(attributeNode.InnerText.ToString()))
                                {
                                    bowling1Id = attributeNode.InnerText.ToString();
                                    if (players.ContainsKey(Convert.ToInt32(bowling1Id)))
                                    {
                                        bowling1Name = ((Player)players[Convert.ToInt32(bowling1Id)]).Name;
                                    }
                                }
                                break;
                            case "bwl2":
                                if (!string.IsNullOrEmpty(attributeNode.InnerText.ToString()))
                                {
                                    bowling2Id = attributeNode.InnerText.ToString();
                                    if (players.ContainsKey(Convert.ToInt32(bowling2Id)))
                                    {
                                        bowling2Name = ((Player)players[Convert.ToInt32(bowling2Id)]).Name;
                                    }
                                }
                                break;
                            case "bwl1wkts":
                                bowling1Wkts = attributeNode.InnerText.ToString();
                                break;
                            case "bwl2wkts":
                                bowling2Wkts = attributeNode.InnerText.ToString();
                                break;
                            case "maxovers":
                                maxOvers = attributeNode.InnerText.ToString();
                                break;
                            case "target":
                                target = attributeNode.InnerText.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    curTarget = target;
                    curOvers = maxOvers;
                    overs = CorrectOvers(overs);

                    sb.Append("INSERT INTO cricket.dbo.scorecard_current (MatchId, Innings, BattingId, BattingName, BowlingId, BowlingName, Runs, Wickets, Overs, Batter1Id, Batter1Name, Batter1Runs, Batter2Id, Batter2Name, Batter2Runs, Bowler1Id, Bowler1Name, Bowler1Wickets, Bowler2Id, Bowler2Name, Bowler2Wickets, MaxOvers, Target) VALUES (" + matchId + ", '" + inning.Replace("'", "''") + "', '" + battingId.Replace("'", "''") + "', '" + battingName.Replace("'", "''") + "', '" + bowlingId.Replace("'", "''") + "', '" + bowlingName.Replace("'", "''") + "', '" + runs.Replace("'", "''") + "', '" + wickets.Replace("'", "''") + "', '" + overs.Replace("'", "''") + "', '" + batting1Id.Replace("'", "''") + "', '" + batting1Name.Replace("'", "''") + "', '" + batting1Runs.Replace("'", "''") + "', '" + batting2Id.Replace("'", "''") + "', '" + batting2Name.Replace("'", "''") + "', '" + batting2Runs.Replace("'", "''") + "', '" + bowling1Id.Replace("'", "''") + "', '" + bowling1Name.Replace("'", "''") + "', '" + bowling1Wkts.Replace("'", "''") + "', '" + bowling2Id.Replace("'", "''") + "', '" + bowling2Name.Replace("'", "''") + "', '" + bowling2Wkts.Replace("'", "''") + "', '" + maxOvers.Replace("'", "''") + "', '" + target.Replace("'", "''") + "');");
                }

                foreach (XmlNode inningsNode in xmlDoc.SelectNodes("scorecard/innings"))
                {
                    int inning = -1;
                    int teamId = -1;
                    string teamName = string.Empty;
                    string runs = string.Empty;
                    string overs = string.Empty;
                    string wickets = string.Empty;
                    string maxOvers = string.Empty;
                    string declared = string.Empty;
                    string minutes = string.Empty;
                    string total = string.Empty;
                    string wides = string.Empty;
                    string noBalls = string.Empty;
                    string byes = string.Empty;
                    string legByes = string.Empty;
                    string penalty = string.Empty;
                    bool insert = true;

                    foreach (XmlAttribute xmlAttribute in inningsNode.Attributes)
                    {
                        switch (xmlAttribute.Name)
                        {
                            case "team":
                                teamId = Convert.ToInt32(xmlAttribute.Value.ToString());
                                teamName = teams[teamId].ToString();
                                break;
                            case "runs":
                                runs = xmlAttribute.Value.ToString();
                                break;
                            case "overs":
                                overs = xmlAttribute.Value.ToString();
                                overs = CorrectOvers(overs);
                                break;
                            case "wkts":
                                wickets = xmlAttribute.Value.ToString();
                                break;
                            case "maxovers":
                                maxOvers = xmlAttribute.Value.ToString();
                                break;
                            case "declared":
                                declared = xmlAttribute.Value.ToString();
                                break;
                            case "minutes":
                                minutes = xmlAttribute.Value.ToString();
                                break;
                            case "number":
                                if (!string.IsNullOrEmpty(xmlAttribute.Value.ToString()))
                                {
                                    inning = Convert.ToInt32(xmlAttribute.Value.ToString()) + 1;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode extraNode in inningsNode.SelectNodes("extras"))
                    {
                        foreach (XmlAttribute xmlAttribute in extraNode.Attributes)
                        {
                            switch (xmlAttribute.Name)
                            {
                                case "total":
                                    total = xmlAttribute.Value.ToString();
                                    break;
                                case "wides":
                                    wides = xmlAttribute.Value.ToString();
                                    break;
                                case "noballs":
                                    noBalls = xmlAttribute.Value.ToString();
                                    break;
                                case "byes":
                                    byes = xmlAttribute.Value.ToString();
                                    break;
                                case "legbyes":
                                    legByes = xmlAttribute.Value.ToString();
                                    break;
                                case "pen":
                                    penalty = xmlAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if ((overs == "0.0" && minutes == "0") || (overs == "0" && minutes == "0") || (teamName == ""))
                    {
                        insert = false;
                    }

                    if (insert)
                    {
                        Innings curInnings = new Innings();
                        curInnings.TeamId = teamId;
                        curInnings.TeamName = teamName;
                        curInnings.Runs = Convert.ToInt32(runs);
                        curInnings.Overs = overs;
                        curInnings.MaxOvers = CorrectOvers(maxOvers);
                        curInnings.Wickets = Convert.ToInt32(wickets);
                        curInnings.Declared = Convert.ToInt32(declared);
                        innings.Add(inning, curInnings);

                        sb.Append("INSERT INTO Cricket.dbo.Scorecard_Innings (MatchId, Innings, Id, Name, Runs, Overs, Wickets, MaxOvers, Declared, Minutes, Extras, Wides, NoBalls, Byes, LegByes, Penalty) VALUES (" + matchId + ", " + inning + ", " + teamId + ", '" + teamName.Replace("'", "''") + "', '" + runs.Replace("'", "''") + "', '" + overs.Replace("'", "''") + "', '" + wickets.Replace("'", "''") + "', '" + maxOvers.Replace("'", "''") + "', '" + declared.Replace("'", "''") + "', '" + minutes.Replace("'", "''") + "', '" + total.Replace("'", "''") + "', '" + wides.Replace("'", "''") + "', '" + noBalls.Replace("'", "''") + "', '" + byes.Replace("'", "''") + "', '" + legByes.Replace("'", "''") + "', '" + penalty.Replace("'", "''") + "');");
                    }

                    foreach (XmlNode battingNode in inningsNode.SelectNodes("batsmen/batsman"))
                    {
                        int batsmanOrder = -1;
                        int batsmanId = 0;
                        string batsmanName = "unkwown";
                        string batsmanRuns = string.Empty;
                        string batsmanBalls = string.Empty;
                        string batsmanFours = string.Empty;
                        string batsmanSixes = string.Empty;
                        string batsmanMinutes = string.Empty;
                        string batsmanHowOut = string.Empty;
                        int batsmanbowlerId = 0;
                        string batsmanbowlerName = "unkwown";
                        int batsmanfielderId = 0;
                        string batsmanfielderName = string.Empty;
                        string tmpBatsmanbowlerName = "";
                        string tmpBatsmanfielderName = "";

                        foreach (XmlAttribute xmlAttribute in battingNode.Attributes)
                        {
                            switch (xmlAttribute.Name)
                            {
                                case "order":
                                    batsmanOrder = Convert.ToInt32(xmlAttribute.Value.ToString());
                                    break;
                                case "id":
                                    if (!string.IsNullOrEmpty(xmlAttribute.Value.ToString()))
                                    {
                                        batsmanId = Convert.ToInt32(xmlAttribute.Value.ToString());
                                        if (players.ContainsKey(batsmanId))
                                        {
                                            batsmanName = ((Player)players[batsmanId]).Name;
                                        }
                                    }
                                    break;
                                case "runs":
                                    batsmanRuns = xmlAttribute.Value.ToString();
                                    break;
                                case "balls":
                                    batsmanBalls = xmlAttribute.Value.ToString();
                                    break;
                                case "fours":
                                    batsmanFours = xmlAttribute.Value.ToString();
                                    break;
                                case "sixes":
                                    batsmanSixes = xmlAttribute.Value.ToString();
                                    break;
                                case "minutes":
                                    batsmanMinutes = xmlAttribute.Value.ToString();
                                    break;
                                case "howout":
                                    batsmanHowOut = xmlAttribute.Value.ToString();
                                    batsmanHowOut = batsmanHowOut.Replace("stumped", "st");
                                    batsmanHowOut = batsmanHowOut.Replace("not out", "notout");
                                    batsmanHowOut = batsmanHowOut.Replace("run out", "runout");
                                    batsmanHowOut = batsmanHowOut.Replace("retired notout", "retired not out");
                                    break;
                                case "bowler":
                                    if (!string.IsNullOrEmpty(xmlAttribute.Value.ToString()))
                                    {
                                        batsmanbowlerId = Convert.ToInt32(xmlAttribute.Value.ToString());
                                        if (players.ContainsKey(batsmanbowlerId))
                                        {
                                            batsmanbowlerName = ((Player)players[batsmanbowlerId]).Name;
                                        }
                                    }
                                    break;
                                case "fielder":
                                    string tmpFielder = xmlAttribute.Value.ToString();
                                    if (!string.IsNullOrEmpty(tmpFielder))
                                    {
                                        string[] tmpArray = tmpFielder.Split(',');
                                        batsmanfielderId = Convert.ToInt32(tmpArray[0]);
                                        if (players.ContainsKey(batsmanfielderId))
                                        {
                                            batsmanfielderName = ((Player)players[batsmanfielderId]).Name;
                                        }
                                    }
                                    break;
                                case "bowlername":
                                    tmpBatsmanbowlerName = xmlAttribute.Value.ToString();
                                    break;
                                case "fieldername":
                                    tmpBatsmanfielderName = xmlAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }

                            if ((batsmanbowlerName == "unkwown" || batsmanbowlerName == "") && tmpBatsmanbowlerName != "")
                            {
                                batsmanbowlerName = tmpBatsmanbowlerName;
                            }
                            if (String.IsNullOrEmpty(batsmanfielderName) && !String.IsNullOrEmpty(tmpBatsmanfielderName))
                            {
                                batsmanfielderName = tmpBatsmanfielderName;
                            }
                        }

                        sb.Append("INSERT INTO Cricket.dbo.Scorecard_Batting (MatchId, Innings, Ordering, Id, Name, Runs, Balls, Fours, Sixes, Minutes, HowOut, BowlerId, BowlerName, FielderId, FielderName) VALUES (" + matchId + ", " + inning + ", " + batsmanOrder + ", " + batsmanId + ", '" + batsmanName.Replace("'", "''") + "', '" + batsmanRuns.Replace("'", "''") + "', '" + batsmanBalls.Replace("'", "''") + "', '" + batsmanFours.Replace("'", "''") + "', '" + batsmanSixes.Replace("'", "''") + "', '" + batsmanMinutes.Replace("'", "''") + "', '" + batsmanHowOut.Replace("'", "''") + "', " + batsmanbowlerId + ", '" + batsmanbowlerName.Replace("'", "''") + "', " + batsmanfielderId + ", '" + batsmanfielderName.Replace("'", "''") + "')");
                    }

                    foreach (XmlNode bowlerNode in inningsNode.SelectNodes("bowlers/bowler"))
                    {
                        int bowlerOrder = -1;
                        int bowlerId = 0;
                        string bowlerName = "unkwown";
                        string bowlerOvers = string.Empty;
                        string bowlerMaidens = string.Empty;
                        string bowlerRunsOff = string.Empty;
                        string bowlerWickets = string.Empty;
                        string bowlerWides = string.Empty;
                        string bowlerNoBalls = string.Empty;

                        foreach (XmlAttribute xmlAttribute in bowlerNode.Attributes)
                        {
                            switch (xmlAttribute.Name)
                            {
                                case "order":
                                    bowlerOrder = Convert.ToInt32(xmlAttribute.Value.ToString());
                                    break;
                                case "id":
                                    if (!string.IsNullOrEmpty(xmlAttribute.Value.ToString()))
                                    {
                                        bowlerId = Convert.ToInt32(xmlAttribute.Value.ToString());
                                        if (players.ContainsKey(bowlerId))
                                        {
                                            bowlerName = ((Player)players[bowlerId]).Name;
                                        }
                                    }
                                    break;
                                case "overs":
                                    bowlerOvers = xmlAttribute.Value.ToString();
                                    bowlerOvers = CorrectOvers(bowlerOvers);
                                    break;
                                case "maidens":
                                    bowlerMaidens = xmlAttribute.Value.ToString();
                                    break;
                                case "runs":
                                    bowlerRunsOff = xmlAttribute.Value.ToString();
                                    break;
                                case "wkts":
                                    bowlerWickets = xmlAttribute.Value.ToString();
                                    break;
                                case "wides":
                                    bowlerWides = xmlAttribute.Value.ToString();
                                    break;
                                case "noballs":
                                    bowlerNoBalls = xmlAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        sb.Append("INSERT INTO Cricket.dbo.Scorecard_Bowling (MatchId, Innings, Ordering, Id, Name, Overs, Maidens, Runsoff, Wickets, Wides, NoBalls) VALUES (" + matchId + ", " + inning + ", " + bowlerOrder + ", " + bowlerId + ", '" + bowlerName.Replace("'", "''") + "', '" + bowlerOvers.Replace("'", "''") + "', '" + bowlerMaidens.Replace("'", "''") + "', '" + bowlerRunsOff.Replace("'", "''") + "', '" + bowlerWickets.Replace("'", "''") + "', '" + bowlerWides.Replace("'", "''") + "', '" + bowlerNoBalls.Replace("'", "''") + "')");
                    }

                    foreach (XmlNode wicketNode in inningsNode.SelectNodes("wickets/wicket"))
                    {
                        int wicketOrder = -1;
                        int wicketBatsmanId = 0;
                        string wicketBatsmanName = "unkwown";
                        string wicketRuns = string.Empty;
                        string wicketOvers = string.Empty;

                        foreach (XmlAttribute xmlAttribute in wicketNode.Attributes)
                        {
                            switch (xmlAttribute.Name)
                            {
                                case "order":
                                    wicketOrder = Convert.ToInt32(xmlAttribute.Value.ToString());
                                    break;
                                case "batsman":
                                    if (!string.IsNullOrEmpty(xmlAttribute.Value.ToString()))
                                    {
                                        wicketBatsmanId = Convert.ToInt32(xmlAttribute.Value.ToString());
                                        if (players.ContainsKey(wicketBatsmanId))
                                        {
                                            wicketBatsmanName = ((Player)players[wicketBatsmanId]).Name;
                                        }
                                    }
                                    break;
                                case "overs":
                                    wicketOvers = xmlAttribute.Value.ToString();
                                    wicketOvers = CorrectOvers(wicketOvers);
                                    break;
                                case "runs":
                                    wicketRuns = xmlAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        sb.Append("INSERT INTO Cricket.dbo.Scorecard_Wickets (MatchId, Innings, Ordering, Id, Name, Runs, Overs) VALUES (" + matchId + ", " + inning + ", " + wicketOrder + ", " + wicketBatsmanId + ", '" + wicketBatsmanName.Replace("'", "''") + "', '" + wicketRuns.Replace("'", "''") + "', '" + wicketOvers.Replace("'", "''") + "')");
                    }
                }

                updateMatchStatus(matchId, innings, result, curTarget, curOvers);

                if (sb.ToString() != "")
                {
                    sb.Insert(0, "DELETE FROM cricket.dbo.scorecard_batting WHERE (MatchId = " + matchId + ");");
                    sb.Insert(0, "DELETE FROM cricket.dbo.scorecard_bowling WHERE (MatchId = " + matchId + ");");
                    sb.Insert(0, "DELETE FROM cricket.dbo.scorecard_wickets WHERE (MatchId = " + matchId + ");");
                    sb.Insert(0, "DELETE FROM cricket.dbo.scorecard_innings WHERE (MatchId = " + matchId + ");");
                    sb.Insert(0, "DELETE FROM cricket.dbo.scorecard_current WHERE (MatchId = " + matchId + ");");
                    SqlQuery = new SqlCommand(sb.ToString(), dbConn);
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in Scorecard()", ex);

                curRun.Errors.Add("Error with scorecard for id:" + matchId + " - " + ex.Message);
            }
        }

        private void updateFixtures(DateTime matchdate, string homeTeam, int matchId)
        {
            homeTeam = homeTeam.Replace("T20", "").Trim();
            SqlCommand SqlQuery = new SqlCommand("SELECT a.cr_Id From SchoolCricket.dbo.cr_fixtures a INNER JOIN SchoolCricket.dbo.cr_teams b ON b.cr_Id = a.cr_teamA_ID WHERE (Year(a.cr_date_start) = " + matchdate.Year + " AND Month(a.cr_date_start) = " + matchdate.Month + " AND Day(a.cr_date_start) = " + matchdate.Day + ") AND (b.cr_team = '" + homeTeam.Replace("'", "''") + "' OR b.cr_team_sn = '" + homeTeam.Replace("'", "''") + "')", dbConn);
            SqlDataReader RsRec = SqlQuery.ExecuteReader();
            ArrayList tmpIds = new ArrayList();
            if (RsRec.HasRows)
            {
                while (RsRec.Read())
                {
                    tmpIds.Add(RsRec["cr_Id"].ToString());
                }
            }
            RsRec.Close();

            if (tmpIds.Count > 0)
            {
                string dbConnStringSchools = ConfigurationManager.AppSettings["sqlSchools"];
                SqlConnection dbConnSchools = new SqlConnection(dbConnStringSchools);
                dbConnSchools.Open();
                for (int i = 0; i <= tmpIds.Count - 1; i++)
                {
                    SqlQuery = new SqlCommand("UPDATE SchoolCricket.dbo.cr_fixtures SET cr_Scorecard = " + matchId + " WHERE (cr_Id = " + tmpIds[i] + ")", dbConnSchools);
                    SqlQuery.ExecuteNonQuery();
                }
                dbConnSchools.Close();
            }
        }

        private void updateMatchStatus(int id, Hashtable inningsArray, bool result, string curTarget, string curOvers)
        {
            string type = string.Empty;
            string series = string.Empty;
            string status = string.Empty;
            string team1 = string.Empty;
            string team2 = string.Empty;
            string score1 = string.Empty;
            string score2 = string.Empty;
            int team1Total = 0;
            int team2Total = 0;
            int count = 0;
            int innings = inningsArray.Count;

            SqlCommand SqlQuery = new SqlCommand("SELECT Type, Series From Cricket.dbo.Scorecard_Matches WHERE (Id = @id)", dbConn);
            SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id;
            SqlDataReader RsRec = SqlQuery.ExecuteReader();
            while (RsRec.Read())
            {
                series = RsRec["Series"].ToString();
                type = RsRec["Type"].ToString().ToLower();
            }
            RsRec.Close();

            for (int i = 1; i <= innings; i++)
            {
                Innings curInnings = (Innings)inningsArray[i];
                bool tmpStatus = true;
                if (curInnings.Wickets == 10 || curInnings.Declared == 1)
                {
                    tmpStatus = false;
                }
                count += 1;

                if (string.IsNullOrEmpty(team1))
                {
                    team1 = curInnings.TeamName;
                    team1Total = curInnings.Runs;
                    if (tmpStatus)
                    {
                        score1 += team1 + " " + curInnings.Runs + "/" + curInnings.Wickets;
                    }
                    else
                    {
                        if (curInnings.Declared == 1)
                        {
                            score1 += team1 + " " + curInnings.Runs + "/" + curInnings.Wickets + " dec";
                        }
                        else
                        {
                            score1 += team1 + " " + curInnings.Runs;
                        }
                    }
                }
                else if (string.IsNullOrEmpty(team2))
                {
                    team2 = curInnings.TeamName;
                    team2Total = curInnings.Runs;
                    if (tmpStatus)
                    {
                        score2 += team2 + " " + curInnings.Runs + "/" + curInnings.Wickets;
                    }
                    else
                    {
                        if (curInnings.Declared == 1)
                        {
                            score2 += team2 + " " + curInnings.Runs + "/" + curInnings.Wickets + " dec";
                        }
                        else
                        {
                            score2 += team2 + " " + curInnings.Runs;
                        }
                    }
                }
                else
                {
                    if (curInnings.TeamName == team1)
                    {
                        team1Total += curInnings.Runs;
                        if (tmpStatus)
                        {
                            score1 += " & " + curInnings.Runs + "/" + curInnings.Wickets;
                        }
                        else
                        {
                            if (curInnings.Declared == 1)
                            {
                                score1 += " & " + curInnings.Runs + "/" + curInnings.Wickets + " dec";
                            }
                            else
                            {
                                score1 += " & " + curInnings.Runs;
                            }
                        }
                    }
                    else
                    {
                        team2Total += curInnings.Runs;
                        if (tmpStatus)
                        {
                            score2 += " & " + curInnings.Runs + "/" + curInnings.Wickets;
                        }
                        else
                        {
                            if (curInnings.Declared == 1)
                            {
                                score2 += " & " + curInnings.Runs + "/" + curInnings.Wickets + " dec";
                            }
                            else
                            {
                                score2 += " & " + curInnings.Runs;
                            }
                        }
                    }
                }

                if (count == innings && result == false)
                {
                    if (type == "test" || type == "first-class" || series == "SAA Provincial Challenge" || series == "SuperSport Series" || series == "Sunfoil Series" || series == "Sunfoil Series")
                    {
                        int tmpWickets = 10 - curInnings.Wickets;
                        if (innings == 1)
                        {
                            if (curInnings.Wickets == 10)
                            {
                                status = curInnings.TeamName + " are " + curInnings.Runs + " all out";
                            }
                            else
                            {
                                status = curInnings.TeamName + " are " + curInnings.Runs + " for " + curInnings.Wickets + " after " + curInnings.Overs + " overs";
                            }
                        }
                        else if (innings == 2 || innings == 3)
                        {
                            int tmpTotal;
                            bool trailing = true;
                            string scores;
                            if (curInnings.TeamName == team1)
                            {
                                if (team1Total > team2Total)
                                {
                                    tmpTotal = team1Total - team2Total;
                                    trailing = false;
                                }
                                else if (team2Total > team1Total)
                                {
                                    tmpTotal = team2Total - team1Total;
                                    trailing = true;
                                }
                                else
                                {
                                    tmpTotal = 0;
                                }
                                scores = score1;
                            }
                            else
                            {
                                if (team2Total > team1Total)
                                {
                                    tmpTotal = team2Total - team1Total;
                                    trailing = false;
                                }
                                else if (team1Total > team2Total)
                                {
                                    tmpTotal = team1Total - team2Total;
                                    trailing = true;
                                }
                                else
                                {
                                    tmpTotal = 0;
                                }
                                scores = score2;
                            }
                            if (tmpTotal == 0 && score1.IndexOf("/0") < 0 && tmpWickets == 0)
                            {
                                status = "Scores tied";
                            }
                            else
                            {
                                string inningsText = "first";
                                if (scores.IndexOf("&") > 0)
                                {
                                    inningsText = "second";
                                }
                                if (trailing)
                                {
                                    status = curInnings.TeamName + " are " + tmpTotal + " runs behind with " + tmpWickets + " " + "wkts left";
                                }
                                else
                                {
                                    status = curInnings.TeamName + " are " + tmpTotal + " runs ahead with " + tmpWickets + " " + "wkts left";
                                }
                            }
                        }
                        else
                        {
                            int tmpTotal;
                            if (curInnings.TeamName == team1)
                            {
                                tmpTotal = team2Total - team1Total;
                            }
                            else
                            {
                                tmpTotal = team1Total - team2Total;
                            }
                            if (tmpTotal < 0)
                            {
                                tmpTotal = 0;
                            }
                            if (tmpTotal > 0)
                            {
                                if(tmpWickets > 1)
                                {
                                    status = curInnings.TeamName + " need another " + (tmpTotal + 1) + " runs for victory with " + tmpWickets + " wkts left";
                                }
                                else
                                {
                                    status = curInnings.TeamName + " need another " + (tmpTotal + 1) + " runs for victory with " + tmpWickets + " wkt left";
                                }
                                

                                
                            }
                            else
                            {
                                if (tmpWickets > 1)
                                {

                                    status = curInnings.TeamName + " need another " + tmpTotal + " runs for victory with " + tmpWickets + " wkts left";
                                }
                                else
                                {
                                    status = curInnings.TeamName + " need another " + tmpTotal + " runs for victory with " + tmpWickets + " wkt left";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (count == 1)
                        {
                            if (curInnings.Wickets == 10)
                            {
                                status = curInnings.TeamName + " all out for " + curInnings.Runs + " runs";
                            }
                            else
                            {
                                status = curInnings.TeamName + " are " + curInnings.Runs + " for " + curInnings.Wickets + " after " + curInnings.Overs + " overs";
                            }
                        }
                        else
                        {
                            int tmpCurTarget = 0;
                            Int32.TryParse(curTarget, out tmpCurTarget);
                            if (tmpCurTarget > 0)
                            {
                                if (Convert.ToInt32(curTarget) != team1Total)
                                {
                                    team1Total = Convert.ToInt32(curTarget) - 1;
                                }
                            }
                            int tmpTotal = team1Total - team2Total;
                            int tmpWickets = 10 - curInnings.Wickets;
                            if (tmpTotal < 0)
                            {
                                tmpTotal = 0;
                            }
                            int ov1;
                            if (curInnings.MaxOvers == "" || curInnings.MaxOvers == "0" || curInnings.MaxOvers == "0.0")
                            {
                                if (type.IndexOf("20") >= 0)
                                {
                                    ov1 = 20;
                                }
                                else if (series == "MTN Domestic Championship")
                                {
                                    ov1 = 45;
                                }
                                else if (series == "Standard Bank Pro20 Series")
                                {
                                    ov1 = 20;
                                }
                                else
                                {
                                    ov1 = 50;
                                }
                            }
                            else
                            {
                                ov1 = Convert.ToInt32(curInnings.MaxOvers);
                            }
                            int ov2;
                            string tmpOvers;
                            if (curInnings.Overs.IndexOf(".") >= 0 )
                            {
                                string[] myArray = curInnings.Overs.Split('.');
                                int tmp1;
                                tmp1 = ov1 - Convert.ToInt32(myArray[0]);
                                if (Convert.ToInt32(myArray[1]) > 0)
                                {
                                    tmpOvers = (tmp1 - 1).ToString() + "." + (6 - Convert.ToInt32(myArray[1])).ToString();
                                }
                                else
                                {
                                    tmpOvers = tmp1.ToString();
                                }
                            }
                            else
                            {
                                ov2 = Convert.ToInt32(curInnings.Overs);
                                tmpOvers = (ov1 - ov2).ToString();
                            }
                            if (tmpWickets > 0)
                            {
                                status = curInnings.TeamName + " are " + tmpTotal + " behind with " + tmpWickets + " wkts & " + tmpOvers + " overs left";
                            }
                            else
                            {
                                status = curInnings.TeamName + " are " + tmpTotal + " behind with no wkt & " + tmpOvers + " overs left";
                            }
                        }
                    }
                }
            }

            if (result)
            {
                SqlQuery = new SqlCommand("UPDATE cricket.dbo.Scorecard_Matches Set Scores1 = @scores1, Scores2 = @scores2 WHERE (id = @id)", dbConn);
                SqlQuery.Parameters.Add("@scores1", SqlDbType.VarChar).Value = score1;
                SqlQuery.Parameters.Add("@scores2", SqlDbType.VarChar).Value = score2;
                SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id;
                SqlQuery.ExecuteNonQuery();
            }
            else
            {
                SqlQuery = new SqlCommand("UPDATE cricket.dbo.Scorecard_Matches Set Scores1 = @scores1, Scores2 = @scores2, MatchStatus = @status WHERE (id = @id)", dbConn);
                SqlQuery.Parameters.Add("@scores1", SqlDbType.VarChar).Value = score1;
                SqlQuery.Parameters.Add("@scores2", SqlDbType.VarChar).Value = score2;
                SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = id;
                SqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                SqlQuery.ExecuteNonQuery();
            }
        }

        private string getFile(string location)
        {
            string fileText = string.Empty;
            string baseUrl = "http://feeds.cricinfo.com/supersports/";
            string url = baseUrl + location;
            NetworkCredential networkCredential = new NetworkCredential(ConfigurationManager.AppSettings["cricUser"], ConfigurationManager.AppSettings["cricPassword"]);

            curRun.Downloads += 1;

            try
            {
                WebRequest myRequest = WebRequest.Create(url);
                //System.Net.IWebProxy iwpxy = WebRequest.GetSystemWebProxy();
                //myRequest.Proxy = iwpxy;
                ////WebProxy wp = new WebProxy();
                ////wp.UseDefaultCredentials = true;
                ////myRequest.Proxy = wp;

                string useProxy = ConfigurationManager.AppSettings["cricUser"].ToString();
                if (useProxy == "true")
                {
                    System.Net.WebProxy tmpProxy;
                    System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
                    tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
                    tmpProxy.Credentials = ProxyCredentials;
                    myRequest.Proxy = tmpProxy;
                }

                myRequest.Timeout = 60000;
                myRequest.PreAuthenticate = true;
                myRequest.Credentials = networkCredential;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                fileText = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in getFile()", ex);

                curRun.DownloadErrors += 1;
                curRun.Errors.Add("Failed to download file - " + location);
                curRun.Errors.Add(ex.Message);
            }

            return fileText.Replace("&","and");
        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in dbConnChanged()", ex);

                        dbConnected = false;
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            run();
        }

        private string CorrectOvers(string overs)
        {
            string retText = "";

            if (overs.IndexOf(".0") >= 0)
            {
                retText = overs.Replace(".0", "");
            }
            else
            {
                retText = overs;
            }

            return retText;
        }
    }

    public class runInfo
    {
        public DateTime Start;
        public DateTime End;
        public int Downloads;
        public int DownloadErrors;
        public List<string> Errors = new List<string>();
    }

    public class Player
    {
        public int Id;
        public int TeamId;
        public string TeamName = string.Empty;
        public string Name = string.Empty;
        public string Surname = string.Empty;
        public string Initials = string.Empty;
        public string AltName = string.Empty;
        public string AltSurname = string.Empty;
        public string AltInitials = string.Empty;
        public bool Captain = false;
        public bool Keeper = false;

        public Player()
        {

        }
    }

    public class Innings
    {
        public int TeamId;
        public string TeamName = string.Empty;
        public int Runs;
        public int Wickets;
        public string Overs = string.Empty;
        public string MaxOvers = string.Empty;
        public int Declared;

        public Innings()
        {

        }
    }
}