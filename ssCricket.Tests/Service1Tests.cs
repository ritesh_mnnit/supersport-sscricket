﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ssCricket.Tests
{
    [TestFixture]
    public class Service1Tests
    {
        [Test]
        public void run_MakingASingleCall_TheCallSucceeds()
        {
            // ARRANGE
            var serviceTestHelper = new Service1TestHelper();

            // ACT

            serviceTestHelper.OnStart(null);

            serviceTestHelper.run();

            // ASSERT
        }
    }

    internal class Service1TestHelper: Service1
    {
        internal new void OnStart(string[] args)
        {
            base.OnStart(null);

            this.sysTimer.Enabled = false;
        }

        internal new protected void run()
        {
            base.run();

            this.sysTimer.Enabled = false;
        }
    }
}
